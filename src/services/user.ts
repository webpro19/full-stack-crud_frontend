import type User from "@/types/user";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}
function saveUser(user: User) {
  return http.post("/users", user);
}
function updateUsers(id: number, user: User) {
  return http.patch(`/users/${id}`, user);
}
function deleteUsers(id: number) {
  return http.delete(`/users/${id}`);
}

export default { getUsers, saveUser, updateUsers, deleteUsers };
